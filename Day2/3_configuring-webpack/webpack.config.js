// 1. Entry
// 2. Extensions to be resolved
// 3. Loaders
// 4. Plugins
// 5. Output
// 6. Mode
// 7. Optimization
// 8. DevServer

const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

let production = process.env.NODE_ENV === "production";

let config = {
    entry: {
        index: "./src/index.js",
        home: "./src/home.js",
    },
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "[name].js",
        clean: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"]
                    }
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./index.html", 
            filename: "index.html"
        })
    ],
    mode: "development"
}

if(production) {
    config.mode = "production"
}

module.exports = config;