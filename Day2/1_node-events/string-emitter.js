const EventEmitter = require('events');

class StringEmitter extends EventEmitter {
    static instance;

    constructor() {
        super();
        this.stringArray = ['Hello', 'JavaScript', 'Node JS'];
        this.run();
    }

    static getInstance() {
        if (!StringEmitter.instance) {
            StringEmitter.instance = new StringEmitter();
        }
        return StringEmitter.instance;
    }

    run() {
        setInterval(() => {
            let randomString = this.stringArray[Math.floor(Math.random() * this.stringArray.length)];
            this.emit('data', randomString);
        }, 2000);
    }

    getData() {
        let randomString = this.stringArray[Math.floor(Math.random() * this.stringArray.length)];
        return randomString;
    }

    pushData(cb) {
        setInterval(() => {
            let randomString = this.stringArray[Math.floor(Math.random() * this.stringArray.length)];
            cb(randomString);
        }, 2000);
    }
}

module.exports = StringEmitter;