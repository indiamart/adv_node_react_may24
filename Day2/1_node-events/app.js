const StringEmitter = require('./string-emitter');

const stringEmitter = StringEmitter.getInstance();

// setInterval(() => {
//     const str = stringEmitter.getData();
//     console.log(str);
// }, 2000);

// stringEmitter.pushData((str) => {
//     console.log(str);
// });

// const S1 = (str) => {
//     console.log("S1 - ", str);
// };

// stringEmitter.pushData(S1);

// const S2 = (str) => {
//     console.log("S2 - ", str);
// };

// stringEmitter.pushData(S2);


const S1 = (str) => {
    console.log("S1 - ", str);
};

stringEmitter.on('data', S1);

let count = 0;

const S2 = (str) => {
    console.log("S2 - ", str);
    count++;
    if (count === 3) {
        stringEmitter.off('data', S2);
    }
};

stringEmitter.on('data', S2);