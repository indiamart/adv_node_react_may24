// import React, { Component } from 'react';

// class ComponentWithProps extends Component {
//     constructor(p) {
//         super(p);

//         // Props are readonly
//         // this.props = { name: "Manish" };
//         // this.props.name = "Abhijeet";

//         // Reference Copy - Will not work
//         // this.state = this.props;
//         // this.state.name = "Abhijeet";

//         // Shallow Copy
//         // this.state = { ...this.props };
//         // this.state.name = "Abhijeet";
//         // this.state.address.city = "Mumbai";

//         // Deep Copy
//         this.state = JSON.parse(JSON.stringify(this.props));
//         this.state.name = "Abhijeet";
//         this.state.address.city = "Mumbai";

//         console.log("Ctor, State:", this.state);
//         console.log("Ctor, Props:", this.props);
//     }

//     render() {
//         console.log("Render, State:", this.state);
//         console.log("Render, Props:", this.props);

//         return (
//             <div>
//                 <h1>Component With Props</h1>
//             </div>
//         );
//     }
// }

// export default ComponentWithProps;

import React, { useState } from 'react';

const ComponentWithProps = (props) => {
    // console.log(props);

    // const [data, setData] = useState(JSON.parse(JSON.stringify(props)));
    const [data, setData] = useState({ ...props });

    return (
        <div>
            <h1>Component With Props</h1>
        </div>
    );
};

export default ComponentWithProps;

// var p1 = { id: 1, name: "Manish", address: { city: "Pune" }, display: () => { } };
// // Reference Copy
// // var p2 = p1;

// // Shallow Copy
// // var p2 = {};
// // p2.id = p1.id;
// // p2.name = p1.name;
// // p2.address = p1.address;

// // var p2 = { ...p1 };

// var p2 = JSON.parse(JSON.stringify(p1));

// // p2.id = 10;
// // p2.address.city = "Mumbai";

// console.log("Person 1", p1);
// console.log("Person 2", p2);