import React from 'react';

// import ComponentOne from '../2_multi-components/ComponentOne';
// import ComponentTwo from '../2_multi-components/ComponentTwo';

// import ComponentOne from '../4_external-css/comp-one/ComponentOne';
// import ComponentTwo from '../4_external-css/comp-two/ComponentTwo';

// import ComponentOne from '../3_inline-styles/ComponentOne';
// import ComponentTwo from '../3_inline-styles/ComponentTwo';

// import ComponentOne from '../5_external-css/comp-one/ComponentOne';
// import ComponentTwo from '../5_external-css/comp-two/ComponentTwo';

import ComponentWithState from '../6_component-state/ComponentWithState';
import ComponentWithProps from '../7_component-props/ComponentWithProps';

const RootComponent = () => {
  return (
    <div className='container'>
      {/* <ComponentOne />
          <ComponentTwo />   */}

      {/* <ComponentWithState /> */}
      <ComponentWithProps name={"Manish"} address={{ city: "Pune", state: "MH" }} display={() => { }} />
    </div>
  );
};

export default RootComponent;