// import React, { Component } from 'react';

// class ComponentWithState extends Component {
//     constructor() {
//         super();
//         // State must be initialized in the constructor
//         // State must be set to an object or null
//         // this.state = "IndiaMart";
//         this.state = { name: "IndiaMart" };
//         this.message = "Hello";

//         console.log("Ctor, State is: ", this.state);
//     }

//     render() {
//         console.log("Render, State is: ", this.state);

//         return (
//             <div>
//                 <h2 className="text-primary">Component With State</h2>
//                 <h2 className="text-primary">Hello, {this.state.name}</h2>
//                 <h2 className="text-primary">{this.message}</h2>
//             </div>
//         );
//     }
// }

// export default ComponentWithState;

import React, { useState } from 'react';

const ComponentWithState = () => {
    const [data, setData] = useState({ name: "IndiaMart", location: "NCR" });

    console.log(data);

    return (
        <div>
            <h2 className="text-primary">Hello, {data}</h2>
        </div>
    );
};

export default ComponentWithState;