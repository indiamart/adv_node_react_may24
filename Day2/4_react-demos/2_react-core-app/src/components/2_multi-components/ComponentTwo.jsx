import React, { Component } from 'react';

class ComponentTwo extends Component {
    render() {
        return (
            <h1 className='text-success'>
                Hello from Component Two
            </h1>
        );
    }
}

export default ComponentTwo;