import React, { Component } from 'react';

class ComponentOne extends Component {
    render() {
        return (
            <h1 className='text-primary'>
                Hello from Component One
            </h1>
        );
    }
}

export default ComponentOne;