// import React from 'react';

// class HelloComponent extends React.Component {
//     render() {
//         return (
//             <h1>Hello World!</h1>
//         );
//     }
// }

// export { HelloComponent };

// // --------------------------------------------

// import React, { Component } from 'react';

// class HelloComponent extends Component {
//     render() {
//         return (
//             <h1>Hello World!</h1>
//         );
//     }
// }

// export { HelloComponent };

// --------------------------------------------

// import React, { Component } from 'react';

// class HelloComponent extends Component {
//     render() {
//         return (
//             <div>
//                 <h1>Hello World!</h1>
//                 <h1>Hello World!</h1>
//             </div>
//         );
//     }
// }

// export { HelloComponent };

// --------------------------------------------

// import React, { Component, Fragment } from 'react';
// import React, { Component } from 'react';

// class HelloComponent extends Component {
//     render() {
//         return (
//             // <React.Fragment>
//             //     <h1>Hello World!</h1>
//             //     <h1>Hello World!</h1>
//             // </React.Fragment>

//             // <Fragment>
//             //     <h1>Hello World!</h1>
//             //     <h1>Hello World!</h1>
//             // </Fragment>

//             <>
//                 <h1>Hello World!</h1>
//                 <h1>Hello World!</h1>
//             </>
//         );
//     }
// }

// export { HelloComponent };

// --------------------------------------------------

// function HelloComponent() {
//     return (
//         <>
//             <h1>Hello World!</h1>
//             <h1>Function Declaration Syntax!</h1>
//         </>
//     );
// }

// const HelloComponent = function () {
//     return (
//         <>
//             <h1>Hello World!</h1>
//             <h1>Function Expression Syntax!</h1>
//         </>
//     );
// }

// const HelloComponent = () => {
//     console.log("This is Hello Component..");

//     return (
//         <>
//             <h1>Hello World!</h1>
//             <h1>Multiline Arrow Syntax!</h1>
//         </>
//     );
// }

const HelloComponent = () => (
    <>
        <h1>Hello World!</h1>
        <h1>Singleline Arrow Syntax!</h1>
    </>
);

export { HelloComponent };