import React, { Component } from 'react';

const card = { marging: '1em', paddingLeft: 0, border: '2px dashed green' };

class ComponentTwo extends Component {
    render() {
        return (
            <>
                <h1 className='text-success'>
                    Hello from Component Two
                </h1>
                <h1 style={{ marging: '1em', paddingLeft: 0, border: '2px dashed green' }}> Hello from Component One </h1>
                <h1 style={card}> Hello from Component One </h1>
            </>
        );
    }
}

export default ComponentTwo;