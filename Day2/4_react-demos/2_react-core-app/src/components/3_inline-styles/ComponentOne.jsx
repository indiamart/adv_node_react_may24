import React, { Component } from 'react';

const card = { marging: '1em', paddingLeft: 0, border: '2px solid blue' };

class ComponentOne extends Component {
    render() {
        return (
            <>
                <h1 className='text-primary'> Hello from Component One </h1>
                <h1 style={{ marging: '1em', paddingLeft: 0, border: '2px solid blue' }}> Hello from Component One </h1>
                <h1 style={card}> Hello from Component One </h1>
            </>
        );
    }
}

export default ComponentOne;