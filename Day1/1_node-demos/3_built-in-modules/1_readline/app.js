const readline = require('readline');
// console.log(readline);

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// rl.question('What is your name? ', (name) => {
//     console.log(`Hello ${name}`);
//     rl.close();
// });

// rl.question('Enter the first number: ', (input1) => {
//     rl.question('Enter the second number ', (input2) => {
//         var sum = parseInt(input1) + parseInt(input2);
//         console.log(`Sume is: ${sum}`);
//         rl.close();
//     });
// });

// console.log("This is the last line of the script...");

function enterNumber(message) {
    return new Promise((resolve, reject) => {
        rl.question(message, (input) => {
            var data = parseInt(input);
            resolve(data);
        });
    });
}

// enterNumber('Enter the first number: ').then((num1) => {
//     return enterNumber('Enter the second number: ').then((num2) => {
//         var sum = num1 + num2;
//         console.log(`Sum is: ${sum}`);
//         rl.close();
//     });
// });

(async () => {
    var n1 = await enterNumber('Enter the first number: ');
    var n2 = await enterNumber('Enter the second number: ');
    var sum = n1 + n2;
    console.log(`Sum is: ${sum}`);
    rl.close();
})();