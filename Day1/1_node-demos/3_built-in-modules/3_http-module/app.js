// const http = require('http');

// const server = http.createServer((req, res) => {
//     res.write('Hello World');
//     res.end();
// });

// server.listen(3000, () => {
//     console.log('Server is running on port 3000');
// });

// -------------------------------------
const fs = require('fs');
const path = require('path');
const http = require('http');

const filePath = path.join(__dirname, 'public', 'index.html');

const server = http.createServer((req, res) => {
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
            res.statusCode = 500;
            res.write('Internal Server Error');
            res.end();
        } else {
            res.statusCode = 200;
            res.write(data);
            res.end();
        }
    });
});

server.listen(3000, () => {
    console.log('Server is running on port 3000');
});
