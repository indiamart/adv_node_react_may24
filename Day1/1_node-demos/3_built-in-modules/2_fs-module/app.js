const fs = require('fs');
const path = require('path');

const filePath = path.join(__dirname, 'file1.txt');

fs.readFile(filePath, 'utf-8', (err, data) => {
    if (err) {
        console.log(err);
        process.exit(1);
    } else {
        console.log(data);
    }
});