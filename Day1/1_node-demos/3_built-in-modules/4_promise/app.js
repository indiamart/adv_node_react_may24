

// var i = 10;
// console.log("i is: ", i);
// console.log("type of i is: ", typeof i);

// var f = function () { console.log("Hello"); };
// console.log("f is: ", f);
// console.log("type of f is: ", f);

// // Can you create a variable of type number?
// // If yes, you should be able to create a variable of type function also.

// // Can you create a variable of type number inside a function?
// // If yes, you should be able to create a variable of type function inside a function also.

// // function f1() {
// //     function f2() { }
// // }

// // Can you return a variable of type number from a function?
// // If yes, you should be able to return a variable of type function from a function also.   // Closure, HOF, Fn Currying, Module Pattern, Reavling Module Pattern

// // function f1() {
// //     function f2() { }

// //     return f2;
// // }

// // Can you pass a variable of type number to a function?
// // If yes, you should be able to pass a variable of type function to a function also.   // Callbacks

// // document.getElementById('btn').addEventListener('click', function () {});

// // $(document).ready(function () {});

// hello2();

// // Function Declaration
// function hello1() {
//     console.log('Hello');
// }

// // Function Expression
// const hello2 = function () {
//     console.log('Hello');
// };

// const hello3 = new Function('console.log("Hello")');

// const hello4 = () => {
//     console.log('Hello');
// }

// ---------------------------------------------------
// const fs = require('fs');

// // Dev 1
// function getData() {
//     var data = fs.readFileSync('./file1.txt', 'utf-8');
//     return data;
// }

// // Dev 2
// const result = getData();
// console.log(result);
// console.log("Last Line");

// // Dev 1
// function getData(successCB) {
//     fs.readFile('./file1.txt', 'utf-8', (err, data) => {
//         if (err) {
//             console.log(err);
//         } else {
//             successCB(data);
//         }
//     });
// }

// // Dev 2
// getData(function (result) {
//     console.log(result);
// });

// console.log("Last Line");

// // Dev 1
// function getData() {
//     return new Promise((resolve, reject)=>{
//         fs.readFile('./file1.txt', 'utf-8', (err, data) => {
//             if (err) {
//                 reject(err);
//             } else {
//                 resolve(data);
//             }
//         });
//     });
// }

// // Dev 2
// var promise = getData();

// promise.then((result) => {
//     console.log(result);
// }).catch((err) => {
//     console.log(err);
// // });

// // Dev 1
import fs from 'fs';

function getData() {
    return new Promise((resolve, reject) => {
        fs.readFile('./file1.txt', 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// // Dev 2
// (async function () {
//     try {
//         var result = await getData();
//         console.log(result);
//     } catch (err) {
//         console.log(err);
//     }
// })();

// var result = await getData();
// console.log(result);

// async function start() {
//     try {
//         var result = await getData();
//         console.log(result);
//     } catch (err) {
//         console.log(err);
//     }
// };

// console.log("Last Line");

// Will not - Promise is resolved and rejected only once 
function getData() {
    return new Promise((resolve, reject) => {
        setInterval(() => {
            console.log("Inside SetInterval");
            resolve('Hello from GetData');
        }, 2000);
    });
}

var promise = getData();

promise.then((result) => {
    console.log(result);
}).catch((err) => {
    console.log(err);
});