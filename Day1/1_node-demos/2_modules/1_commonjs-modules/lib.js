console.log("Lib Module Loaded...");

// var fname = 'Manish';
// module.exports = fname;

// var lname = 'Sharma';
// module.exports = lname;

var fname = 'Manish';
var lname = 'Sharma';

module.exports.firstname = fname;
module.exports.lastname = lname;

function sayHello(name) {
    console.log('Hello ' + name);
}

module.exports.sayHello = sayHello;

class Employee {
    constructor(name) {
        this._name = name;
    }

    getName() {
        return this._name;
    }

    setName(name) {
        this._name = name;
    }
}

module.exports.Employee = Employee;