class Employee {
    constructor(name) {
        this._name = name;
    }

    getName() {
        return this._name;
    }

    setName(name) {
        this._name = name;
    }
}

export default Employee;

// class Employee {
//     constructor(name) {
//         this._name = name;
//     }

//     getName() {
//         return this._name;
//     }

//     setName(name) {
//         this._name = name;
//     }
// }

// function sayHello(name) {
//     console.log('Hello ' + name);
// }

// export { Employee, sayHello };