import WHatever from "./lib.js";

const emp = new WHatever('John');
console.log(emp.getName());
emp.setName('Jane');
console.log(emp.getName());

// import { Employee, sayHello } from "./lib.js";

// const emp = new Employee('John');
// console.log(emp.getName());
// emp.setName('Jane');
// console.log(emp.getName());

// sayHello('John');


// import * as lib from "./lib.js";

// const emp = new lib.Employee('John');
// console.log(emp.getName());
// emp.setName('Jane');
// console.log(emp.getName());

// lib.sayHello('John');