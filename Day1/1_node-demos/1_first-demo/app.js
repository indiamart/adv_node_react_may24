// console.log("Hello from Node");

// var message = 'Hello from Node';
// console.log(message);
// console.log(typeof message);

// function sayHello(name) {
//     console.log('Hello ' + name);
// }

// sayHello('John');
// sayHello('Jane');

// var Employee = (function() {
//     function Employee(name) {
//         this._name = name;
//     }

//     Employee.prototype.getName = function() {
//         return this._name;
//     }

//     Employee.prototype.setName = function(name) {
//         this._name = name;
//     }

//     return Employee;
// })();

class Employee {
    constructor(name) {
        this._name = name;
    }

    getName() {
        return this._name;
    }

    setName(name) {
        this._name = name;
    }
}

const emp = new Employee('John');
console.log(emp.getName());
emp.setName('Jane');
console.log(emp.getName());