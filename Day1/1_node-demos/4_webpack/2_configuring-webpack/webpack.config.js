// 1. Entry
// 2. Extensions to be resolved
// 3. Loaders
// 4. Plugins
// 5. Output
// 6. Mode
// 7. Optimization
// 8. DevServer

const path = require("path");

module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "code.js"
    },
}