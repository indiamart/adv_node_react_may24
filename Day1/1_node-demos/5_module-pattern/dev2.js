var dev2;

(function (ns) {
    function hello() {
        console.log("Hello from Dev2");
    }

    ns.hello = hello;
})(dev2 = dev2 || {});