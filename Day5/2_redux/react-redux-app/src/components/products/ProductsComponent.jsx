import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ProductListComponent from './ProductListComponent';
import { fetchProducts } from '../../features/products/productsSlice';

const ProductsComponent = () => {
    const dispatch = useDispatch();
    const products = useSelector((state) => state.products.items);

    useEffect(() => {
        dispatch(fetchProducts());
    });

    return (
        <div>
            <ProductListComponent products={products} />
        </div>
    );
};

export default ProductsComponent;