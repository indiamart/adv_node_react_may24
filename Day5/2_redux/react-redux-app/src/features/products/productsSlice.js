import { createSlice } from "@reduxjs/toolkit";
import productAPIClient from '../../services/product-api-client';

export const productsSlice = createSlice({
    name: 'counter',
    initialState: {
        items: []
    },
    reducers: {
        setProducts: (state, action) => {
            state.items = action.payload;
        }
    }
});

export const fetchProducts = (amount) => async (dispatch) => {
    const products = await productAPIClient.getAllProducts();
    dispatch(setProducts(products));
}

const { setProducts } = productsSlice.actions;

export default productsSlice.reducer;