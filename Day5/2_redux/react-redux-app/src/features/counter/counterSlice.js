import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        count: 0
    },
    reducers: {
        increment: state => {
            state.count += 1;
        },
        decrement: state => {
            state.count -= 1;
        },
        incrementBy: (state, action) => {
            state.count += action.payload;
        },
        decrementBy: (state, action) => {
            state.count -= action.payload;
        }
    }
});

export const incrementAsync = (amount) => (dispatch) => {
    setTimeout(() => {
        dispatch(incrementBy(amount));
    }, 1000);
}

export const decrementAsync = (amount) => (dispatch) => {
    setTimeout(() => {
        dispatch(decrementBy(amount));
    }, 1000);
}

// export const incrementAsync = function (amount) {
//     return function (dispatch) {
//         setTimeout(() => {
//             dispatch(incrementBy(amount));
//         }, 1000);
//     };
// }

export const { increment, decrement, incrementBy, decrementBy } = counterSlice.actions;

export default counterSlice.reducer;