// import React, { Component } from 'react';
// import LoaderAnimation from '../common/LoaderAnimation';
// import DataTable from '../common/DataTable';
// import { postApiClient } from '../../services/post-api-client';

// class AjaxComponent extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             data: [],
//             message: "Loading data, please wait...",
//             flag: false
//         }
//     }

//     render() {
//         return (
//             <div>
//                 <div className="row mt-5">
//                     <h4 className="text-warning text-center text-uppercase font-weight-bold">{this.state.message}</h4>
//                 </div>

//                 {
//                     !this.state.flag ? <LoaderAnimation /> : null
//                 }

//                 <DataTable items={this.state.data}>
//                     <h4 className="text-primary text-uppercase font-weight-bold">Posts Table</h4>
//                 </DataTable>
//             </div>
//         );
//     }

//     // componentDidMount() {
//     //     postApiClient.getAllPosts().then(result => {
//     //         this.setState({
//     //             data: result,
//     //             message: "",
//     //             flag: true
//     //         })
//     //     }).catch(error => {
//     //         this.setState({
//     //             message: error,
//     //             flag: true
//     //         });
//     //     });
//     // }

//     async componentDidMount() {
//         try {
//             const result = await postApiClient.getAllPosts();
//             this.setState({ data: result, message: "", flag: true });
//         } catch (err) {
//             this.setState({ data: [], message: err, flag: true });
//         }
//     }

// }

// export default AjaxComponent;

import React, { useEffect, useState } from 'react';
import LoaderAnimation from '../common/LoaderAnimation';
import DataTable from '../common/DataTable';
import { postApiClient } from '../../services/post-api-client';

const AjaxComponent = () => {
    const [cState, setcState] = useState({
        data: [],
        message: "Loading data, please wait...",
        flag: false
    });

    const [count, setCount] = useState(0);

    // With second parameter as empty array, useEffect behaves like ComponentDidMount()
    // useEffect(() => {
    //     console.log("useEffect is called, on mounting");
    // }, []);

    // Without second parameter, useEffect behaves like ComponentDidUpdate()
    // useEffect(() => {
    //     console.log("useEffect is called, on mounting & state updates");
    // });

    // useEffect(() => {
    //     console.log("useEffect is called, on mounting & count state updates");
    // }, [count]);

    // useEffect(() => {
    //     // Anything in here is fired on component mount
    //     console.log("useEffect is called, on mounting");
    //     return () => {
    //         // Anything in here is fired on component unmounts
    //         console.log("useEffect is called, on unmounting");
    //     };
    // }, []);

    // useEffect(() => {
    //     postApiClient.getAllPosts().then(result => {
    //         setcState({
    //             data: result,
    //             message: "",
    //             flag: true
    //         });
    //     }).catch(error => {
    //         setcState({
    //             message: error,
    //             flag: true
    //         });
    //     });
    // }, []);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const result = await postApiClient.getAllPosts();
                setcState({ data: result, message: "", flag: true });
            } catch (err) {
                setcState({ data: [], message: err, flag: true });
            }
        };

        fetchData();
    }, []);

    return (
        <div>
            <div className="row mt-5">
                <h4 className="text-warning text-center text-uppercase font-weight-bold">{cState.message}</h4>
            </div>

            {
                !cState.flag ? <LoaderAnimation /> : null
            }

            <DataTable items={cState.data}>
                <h4 className="text-primary text-uppercase font-weight-bold">Posts Table</h4>
            </DataTable>

            {/* <button className="btn btn-primary" onClick={() => {
                setcState({
                    flag: true
                });
            }}>Update Flag</button>

            <button className="btn btn-primary" onClick={() => {
                setCount(count + 1);
            }}>Update Count</button> */}
        </div>
    );
};

export default AjaxComponent;