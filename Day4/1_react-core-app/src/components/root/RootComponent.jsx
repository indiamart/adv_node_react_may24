import React from 'react';
import CrudAssignment from '../1_assignment/CrudAssignment';
import ContextDemo from '../2_context-api/ContextDemo';
import ReducerHookDemo from '../3_hooks/1_ReducerHookDemo';
import ContextHookDemo from '../3_hooks/2_ContextHookDemo';
import CounterRoot from '../3_hooks/code-structuring/CounterRoot';
import AjaxComponent from '../4_ajax/AjaxComponent';

const RootComponent = () => {
  return (
    <div className='container'>
      {/* <CrudAssignment /> */}
      {/* <ContextDemo /> */}
      {/* <ReducerHookDemo /> */}
      {/* <ContextHookDemo /> */}
      {/* <CounterRoot /> */}
      <AjaxComponent />
    </div>
  );
};

export default RootComponent;