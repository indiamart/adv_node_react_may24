import React, { Component } from 'react';
import DataTable from '../common/DataTable';
import TextInput from '../common/TextInput';
import ConfirmModal from '../common/ConfirmModal';

const FormComponent = (props) => {
    return (
        <>
            <div className='row'>
                <div className='col-sm-6 offset-sm-3'>
                    <form className='form-horizontal' autoComplete='off' onSubmit={
                        (e) => {
                            e.preventDefault();
                            props.saveEmployee();
                        }
                    }>
                        <fieldset disabled={props.disabled}>
                            <legend className="text-center text-secondary text-uppercase font-weight-bold">Add/Edit Employee Information</legend>
                            <hr className="mt-0" />
                            <TextInput label={"Employee ID"} name={"id"} value={props.employee.id} readOnly={true} />
                            <TextInput label={"Employee Name"} name={"name"} value={props.employee.name}
                                onChange={props.changeEmployee} />
                            <TextInput label={"Designation"} name={"designation"} value={props.employee.designation}
                                onChange={props.changeEmployee} />
                            <TextInput label={"Salary"} name={"salary"} value={props.employee.salary}
                                onChange={props.changeEmployee} />

                            <div className="d-grid gap-2 mx-auto mt-3">
                                <button type="submit" className="btn btn-success">Save</button>
                                <button type="reset" className="btn btn-primary" onClick={
                                    (e) => { props.resetEmployee(); }
                                }>Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </>
    );

}

class CrudAssignment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            employee: { id: 1, name: "", designation: "", salary: 0 },
            edit: false,
            formDisabled: false,
            showModal: false,
            deleteCandidate: null,
        };

        this.saveEmployee = this.saveEmployee.bind(this);
        this.removeEmployee = this.removeEmployee.bind(this);
        this.changeEmployee = this.changeEmployee.bind(this);
        this.selectEmployee = this.selectEmployee.bind(this);
        this.resetEmployee = this.resetEmployee.bind(this);
        this.toggleFormDisabled = this.toggleFormDisabled.bind(this);
        this.handleModalYes = this.handleModalYes.bind(this);
        this.handleModalNo = this.handleModalNo.bind(this);
    }

    toggleFormDisabled(disable) {
        this.setState({ formDisabled: disable });
    }

    saveEmployee() {
        if (this.state.edit) {
            var temp_employees = [...this.state.employees];
            var itemIndex = temp_employees.findIndex(e => e.id === parseInt(this.state.employee.id));
            temp_employees.splice(itemIndex, 1, { ...this.state.employee });
            this.setState({
                employees: [...temp_employees],
                employee: { id: this.getNextId(this.state.employees), name: "", designation: "", salary: "" },
                edit: false
            });
        } else {
            const temp_employees = [...this.state.employees, { ...this.state.employee }];
            this.setState({ employees: [...temp_employees], }, () => {
                this.setState({ employee: { id: this.getNextId(this.state.employees), name: "", designation: "", salary: 0 } });
            });
        }
    }

    getNextId(employees) {
        return employees.length ? employees[employees.length - 1].id + 1 : 1;
    }

    selectEmployee(item, allowEdit) {
        if (allowEdit) {
            this.setState({ employee: { ...item }, edit: true, formDisabled: false });
        } else {
            this.setState({ employee: { ...item }, edit: false, formDisabled: true });
        }
    }

    removeEmployee(id) {
        // this.setState({
        //     employees: [...this.state.employees.filter(item => item.id !== id)]
        // }, () => {
        //     this.setState({ employee: { id: this.getNextId(this.state.employees), name: "", designation: "", salary: "" } });
        // });

        this.setState({ showModal: true, deleteCandidate: id });
    }

    handleModalYes() {
        const { deleteCandidate, employees } = this.state;
        const newEmployees = employees.filter(employee => employee.id !== deleteCandidate);
        this.setState({
            employees: newEmployees,
            showModal: false,
            deleteCandidate: null
        }, () => {
            this.setState({ employee: { id: this.getNextId(this.state.employees), name: "", designation: "", salary: "" } });
        });
    }

    handleModalNo() {
        this.setState({
            showModal: false, deleteCandidate: null,
            employee: {
                id: this.getNextId(this.state.employees),
                name: "",
                designation: "",
                salary: ""
            }
        });
    }

    changeEmployee(e) {
        const field = e.target.id;
        var newEmployee = { ...this.state.employee };
        if ((field === "id") && e.target.value)
            newEmployee[field] = parseInt(e.target.value);
        else
            if ((field === "salary") && e.target.value)
                newEmployee[field] = parseInt(e.target.value);
            else
                newEmployee[field] = e.target.value;
        this.setState({ employee: newEmployee });
    }

    resetEmployee() {
        this.setState({ employee: { id: this.getNextId(this.state.employees), name: "", designation: "", salary: "" } });
    }

    render() {
        return (
            <div className='mt-3'>
                <FormComponent employee={this.state.employee} changeEmployee={this.changeEmployee}
                    saveEmployee={this.saveEmployee} resetEmployee={this.resetEmployee}
                    disabled={this.state.formDisabled} />
                <hr />
                <DataTable items={this.state.employees} onSelect={this.selectEmployee} onDelete={this.removeEmployee}>
                    <h5 className="text-primary text-uppercase font-weight-bold">Employees Table</h5>
                </DataTable>
                <ConfirmModal
                    show={this.state.showModal}
                    title="Confirm Delete"
                    message="Are you sure you want to delete this record?"
                    handleYes={this.handleModalYes}
                    handleNo={this.handleModalNo}
                />
            </div>
        );
    }
}

export default CrudAssignment;