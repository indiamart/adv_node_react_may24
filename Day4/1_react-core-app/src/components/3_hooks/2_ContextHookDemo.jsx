import React, { createContext, useContext, useReducer } from 'react';

const counterState = { count: 0 };

const counterReducer = (state, action) => {
    switch (action.type) {
        case counterActions.increment:
            return { count: state.count + action.payload };
        case counterActions.decrement:
            return { count: state.count - action.payload };
        default:
            return state;
    }
};

const counterActions = {
    increment: 'increment',
    decrement: 'decrement'
};

const CounterContext = createContext();

const Counter = (props) => {
    const context = useContext(CounterContext);

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={context.counterState.count} readOnly />
                <button className="btn btn-info"
                    onClick={() => { context.counterDispatch({ type: counterActions.increment, payload: props.interval }) }}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info"
                    onClick={() => { context.counterDispatch({ type: counterActions.decrement, payload: props.interval }) }}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

const CounterSibling = (props) => {
    const context = useContext(CounterContext);

    return (
        <>
            <div className="text-center">
                <h3 className="text-success">Counter Sibling Component</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={context.counterState.count} readOnly />
                <button className="btn btn-info"
                    onClick={() => { context.counterDispatch({ type: counterActions.increment, payload: props.interval }) }}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info"
                    onClick={() => { context.counterDispatch({ type: counterActions.decrement, payload: props.interval }) }}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

const ContextHookDemo = () => {
    const [state, dispatch] = useReducer(counterReducer, counterState);

    return (
        <CounterContext.Provider value={{ counterState: state, counterDispatch: dispatch }}>
            <Counter interval={1} />
            <hr />
            <CounterSibling interval={10} />
        </CounterContext.Provider>
    );
};

export default ContextHookDemo;


// let numArr = [10, 20, 30];

// // function append(numArr, item) {
// //     numArr[numArr.length] = item;
// //     return numArr;
// // }

// function append(numArr, item) {
//     var tempArr = [...numArr]
//     tempArr[tempArr.length] = item;
//     return tempArr;
// }

// let numArr1 = append(numArr, 40);
// console.log(numArr1);

// let numArr2 = append(numArr, 40);
// console.log(numArr2);