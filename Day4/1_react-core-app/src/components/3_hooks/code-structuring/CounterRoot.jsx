import React from 'react';
import { CounterProvider } from './counter-context';
import Counter from './Counter';
import CounterSibling from './CounterSibling';

const CounterRoot = () => {
    return (
        <CounterProvider>
            <Counter interval={1} />
            <hr />
            <CounterSibling interval={10} />
        </CounterProvider>
    );
};

export default CounterRoot;