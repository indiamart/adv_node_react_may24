const url = process.env.REACT_APP_POSTS_API_URL;

const postApiClient = {
    getAllPosts: function () {
        return new Promise((resolve, reject) => {
            fetch(url).then(response => {
                response.json().then(data => {
                    setTimeout(() => {
                        resolve(data);
                    }, 5000);
                }).catch(error => {
                    console.error(error);
                    reject("Parsing Error");
                });
            }).catch(error => {
                console.error(error);
                reject("Communication Error");
            });
        });
    }
};

export { postApiClient };