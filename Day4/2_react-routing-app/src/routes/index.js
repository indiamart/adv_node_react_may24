import React from 'react';
import { Route, Routes, useLocation, Link, useParams, Navigate } from 'react-router-dom';
import HomeComponent from '../components/home/HomeComponent';
import AboutComponent from '../components/about/AboutComponent';
import ProductsComponent from '../components/products/ProductsComponent';
import ProductNotSelectedComponent from '../components/products/ProductNotSelectedComponent';
import ProductDetailsComponent from '../components/products/ProductDetailsComponent';

const productsData = [
    {
        id: 1,
        name: "Item One",
        description:
            "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        status: "Available"
    },
    {
        id: 2,
        name: "Item Two",
        description: "sunt aut facere ptio reprehenderit",
        status: "Not Available"
    },
    {
        id: 3,
        name: "Item Three",
        description: "provident occaecati excepturi optio reprehenderit",
        status: "Available"
    },
    {
        id: 4,
        name: "Item Four",
        description: "reprehenderit",
        status: "Not Available"
    }
];

export default (
    <Routes>
        <Route path="/" element={<HomeComponent />} />
        <Route path="/about" element={<AboutComponent />} />
        <Route path="/products" element={<ProductsComponent productsData={productsData} />}>
            <Route path="" element={<ProductNotSelectedComponent />} />
            <Route path=":productId" element={<ProductDetailsWrapper />} />
        </Route>
        <Route path="not-found" element={<NoMatch />} />
        <Route path="*" element={<NoMatch />} />
    </Routes>
);

function ProductDetailsWrapper() {
    const { productId } = useParams();
    // Check if the id is numeric
    if (!/^\d+$/.test(productId)) {
      return <Navigate to="/not-found" />;
    }
    return <ProductDetailsComponent data={productsData} />;
  }

const img404 = require('../assets/http-404.jpg');

function NoMatch() {
    let location = useLocation();

    return (
        <div className='text-center'>
            <article>
                <h1 className="text-danger">No match found for <code>{location.pathname}</code></h1>
                <h4 className="text-danger">Please check your Route Configuration</h4>
                <div className="mt-5">
                    <img src={img404} alt="Not Found" className="rounded" />
                </div>
                <h2 className="mt-5">
                    <Link className="nav-link" to="/">Back to Home</Link>
                </h2>
            </article>
        </div>
    );
}