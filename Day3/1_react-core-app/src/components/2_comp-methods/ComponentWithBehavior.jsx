// import React, { Component } from 'react';

// class ComponentWithBehavior extends Component {
//     constructor(props) {
//         super(props);
//         this.state = { id: 1, count: 0 };
//     }

//     handleClick() {
//         // alert("Button Clicked");
//         // console.log(this);

//         // this.state.count += 1;
//         // console.log(this.state);

//         this.setState({ count: this.state.count + 1 });
//     }

//     render() {
//         return (
//             <div>
//                 <h2 className="text-primary">Component with Behavior</h2>
//                 <h2 className="text-primary">Id: {this.state.id}</h2>
//                 <h2 className="text-primary">Count: {this.state.count}</h2>

//                 <div className="d-grid gap-2 col-6 mx-auto mt-5">
//                     <button className='btn btn-outline-primary' onClick={this.handleClick.bind(this)}>
//                         Click
//                     </button>
//                 </div>
//             </div>
//         );
//     }
// }

// export default ComponentWithBehavior;


// // var person = {
// //     age: 1,
// //     growOld: function () {
// //         console.log("Grow Old Called...", this);
// //         this.age++;
// //     }
// // };

// // // console.log(person.age);
// // // person.growOld();
// // // console.log(person.age);
// // // person.growOld();
// // // console.log(person.age);

// // // setInterval(person.growOld.bind(person), 1000);

// // document.getElementById('btn').addEventListener('click', person.growOld.bind(person));



import React from 'react';
import { useState } from 'react';

const ComponentWithBehavior = () => {
    const [id, setId] = useState(1);
    const [count, setCount] = useState(0);

    return (
        <div>
            <h2 className="text-primary">Component with Behavior</h2>
            <h2 className="text-primary">Id: {id}</h2>
            <h2 className="text-primary">Count: {count}</h2>

            <div className="d-grid gap-2 col-6 mx-auto mt-5">
                <button className='btn btn-outline-primary' onClick={() => { setCount(count + 1) }}>
                    Click
                </button>
            </div>
        </div>
    );
};

export default ComponentWithBehavior;