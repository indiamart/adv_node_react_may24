import React from 'react';
import PropTypesRoot from '../1_prop-types/PropTypesComponent';
import ComponentWithBehavior from '../2_comp-methods/ComponentWithBehavior';
import EventComponent from '../3_synthetic-event-object/EventComponent';
import CounterAssignment from '../4_assignment/CounterAssignment';
import ControlledVsUncontrolledComponent from '../5_controlled-vs-uncontrolled/ControlledVsUncontrolledComponent';
import CalculatorAssignment from '../6_assignment/CalculatorAssignment';
import ListRoot from '../7_working-with-arrays/ListComponent';

const RootComponent = () => {
  return (
    <div className='container'>
      {/* <PropTypesRoot /> */}
      {/* <ComponentWithBehavior /> */}
      {/* <EventComponent /> */}
      {/* <CounterAssignment /> */}
      {/* <ControlledVsUncontrolledComponent /> */}
      {/* <CalculatorAssignment /> */}
      <ListRoot />
    </div>
  );
};

export default RootComponent;