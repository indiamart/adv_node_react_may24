import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = { count: 0, flag: false };
        this.clickCount = 0;
        this.inc = this.inc.bind(this);
        this.dec = this.dec.bind(this);
        this.reset = this.reset.bind(this);
    }

    manageClickCount() {
        this.clickCount++;
        if (this.clickCount > 9) {
            this.setState({ flag: true }, () => {
                this.props.onMax(this.state.flag);
            });
        }
    }

    inc() {
        this.setState({ count: this.state.count + this.props.interval }, () => {
            this.manageClickCount();
        });
    }

    dec() {
        this.setState({ count: this.state.count - this.props.interval }, () => {
            this.manageClickCount();
        });
    }

    reset() {
        this.setState({ count: 0, flag: false }, () => {
            this.props.onMax(this.state.flag);
        });
        this.clickCount = 0;
    }

    render() {
        return (
            <>
                <div className="text-center">
                    <h3 className="text-info">Counter Component</h3>
                </div>
                <div className="d-grid gap-2 mx-auto col-6">
                    <input type="text" className="form-control form-control-lg" value={this.state.count} readOnly />
                    <button className="btn btn-info" disabled={this.state.flag} onClick={this.inc}>
                        <span className='fs-4'>+</span>
                    </button>
                    <button className="btn btn-info" disabled={this.state.flag} onClick={this.dec}>
                        <span className='fs-4'>-</span>
                    </button>
                    <button className="btn btn-secondary" disabled={!this.state.flag} onClick={this.reset}>
                        <span className='fs-4'>Reset</span>
                    </button>
                </div>
            </>
        );
    }

    static get defaultProps() {
        return {
            interval: 1
        };
    }

    static get propTypes() {
        return {
            interval: PropTypes.number
        };
    }
}

class CounterAssignment extends Component {
    constructor(props) {
        super(props);
        this._counterRef = React.createRef();
        this.p_reset = this.p_reset.bind(this);
        this.updateMessage = this.updateMessage.bind(this);
        this.state = { message: "" };
    }

    p_reset(e) {
        // console.log(this.refs.c1);
        // console.log(this._counterRef);

        if (this._counterRef.current) {
            this._counterRef.current.reset();
        }
    }

    updateMessage(flag) {
        if (flag)
            this.setState({ message: "Max Click Reached, please click reset button to restart" });
        else
            this.setState({ message: "" });
    }

    render() {
        return (
            <div>
                <h2 className="text-success text-center mt-3 mb-3">Calling Child Method from Parent using ref</h2>
                {/* <Counter ref="c1" /> */}

                {
                    this.state.message ? <div className="alert alert-warning">{this.state.message}</div> : null
                }

                <Counter ref={this._counterRef} onMax={this.updateMessage} />

                <hr />
                <div className="text-center">
                    <button className="btn btn-warning btn-block" onClick={this.p_reset}>Parent Reset Button</button>
                </div>
            </div>

            // <div>
            //     <Counter />
            //     <hr />
            //     <Counter interval={10} />
            // </div>
        );
    }
}

export default CounterAssignment;

// // Using Functional
// import React, { useState } from 'react';
// import PropTypes from 'prop-types';
// import { useCallback } from 'react';
// import { useRef } from 'react';

// const Counter = function ({ interval = 1 }) {
//     console.log("Counter Executed....");

//     const [count, setCount] = useState(0);
//     const [flag, setFlag] = useState(false);
//     const clickCount = useRef(0);

//     const manageClickCount = useCallback(() => {
//         clickCount.current++;
//         if (clickCount.current > 9) {
//             setFlag(true);
//         }
//     }, []);

//     // const manageClickCount = () => {
//     //     clickCount.current++;
//     //     if (clickCount.current > 9) {
//     //         setFlag(true);
//     //     }
//     // };

//     const inc = useCallback(() => {
//         setCount(prevCount => prevCount + interval);
//         manageClickCount();
//     }, [interval, manageClickCount]);

//     const dec = useCallback(() => {
//         setCount(prevCount => prevCount - interval);
//         manageClickCount();
//     }, [interval, manageClickCount]);

//     const reset = useCallback(() => {
//         setCount(0);
//         setFlag(false);
//         clickCount.current = 0;
//     }, []);

//     return (
//         <>
//             <div className="text-center">
//                 <h3 className="text-info">Counter Component</h3>
//             </div>
//             <div className="d-grid gap-2 mx-auto col-6">
//                 <input type="text" className="form-control form-control-lg" value={count} readOnly />
//                 <button className="btn btn-info" disabled={flag} onClick={inc}>
//                     <span className='fs-4'>+</span>
//                 </button>
//                 <button className="btn btn-info" disabled={flag} onClick={dec}>
//                     <span className='fs-4'>-</span>
//                 </button>
//                 <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
//                     <span className='fs-4'>Reset</span>
//                 </button>
//             </div>
//         </>
//     );
// };

// Counter.propTypes = {
//     interval: PropTypes.number
// };

// const CounterAssignment = function () {
//     return (
//         <div>
//             <Counter />
//             <hr />
//             <Counter interval={10} />
//         </div>
//     );
// }

// export default CounterAssignment;