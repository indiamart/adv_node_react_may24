// import React, { Component } from 'react';

// class ControlledVsUncontrolledComponent extends Component {
//     constructor(props) {
//         super(props);
//         this.state = { name: "Manish" };
//         this.inputRef = React.createRef();
//         this.handleChange = this.handleChange.bind(this);
//         this.handleClick = this.handleClick.bind(this);
//     }

//     handleChange(e) {
//         this.setState({ name: e.target.value });
//     }

//     handleClick(e) {
//         this.setState({ name: this.inputRef.current.value });
//     }

//     render() {
//         return (
//             <div className='text-center'>
//                 <h1 className="text-info">Controlled & Uncontrolled Component</h1>

//                 <div className="d-grid gap-2 col-6 mx-auto mt-5">
//                     {/* Uncontrolled */}
//                     {/* <input type="text" className='form-control' />
//                     <input type="text" className='form-control' defaultValue={"Abhijeet"} />
//                     <input type="text" className='form-control' defaultValue={this.state.name} /> */}

//                     {/* Controlled */}
//                     {/* <input type="text" className='form-control' value={"Abhijeet"} readOnly />
//                     <input type="text" className='form-control' value={this.state.name} readOnly />
//                     <input type="text" className='form-control' value={this.state.name} onChange={this.handleChange} /> */}

//                     <input type="text" className='form-control' defaultValue={this.state.name} ref={this.inputRef}/>
//                     <h2 className="text-info">Name is: {this.state.name}</h2>
//                     <button className='btn btn-primary' onClick={this.handleClick}>Click</button>
//                 </div>
//             </div>
//         );
//     }
// }

// export default ControlledVsUncontrolledComponent;

import React, { useRef, useState } from 'react';

const ControlledVsUncontrolledComponent = () => {
    const [name, setName] = useState("Manish");
    const inputRef = useRef(null);

    const handleClick = (e) => {
        setName(inputRef.current.value);
    };

    return (
        <div>
            <input type="text" className='form-control' defaultValue={name} ref={inputRef} />
            <h2 className="text-info">Name is: {name}</h2>
            <button className='btn btn-primary' onClick={handleClick}>Click</button>
        </div>
    );
};

export default ControlledVsUncontrolledComponent;