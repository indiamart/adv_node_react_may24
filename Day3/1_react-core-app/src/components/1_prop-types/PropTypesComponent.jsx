// import React, { Component } from 'react';

// // class PropTypesComponent extends Component {
// //     constructor(p) {
// //         super(p);
// //         console.log(this.props);
// //     }

// //     render() {
// //         return (
// //             <div>
// //                 <h2 className="text-primary">Hello, {this.props.name.toUpperCase()}</h2>
// //                 <h2 className="text-primary">You are from, {this.props.city}</h2>
// //             </div>
// //         );
// //     }

// //     static get defaultProps() {
// //         return {
// //             name: "Manish",
// //             city: "Pune"
// //         };
// //     }
// // }

// function PropTypesComponent({ name = "na", city = "" }) {
//     return (
//         <div>
//             <h2 className="text-primary">Hello, {name.toUpperCase()}</h2>
//             <h2 className="text-primary">You are from, {city}</h2>
//         </div>
//     );
// }

// class PropTypesRoot extends Component {
//     render() {
//         return (
//             <div>
//                 <PropTypesComponent />
//                 <hr />
//                 <PropTypesComponent name={"RK"} />
//                 <hr />
//                 <PropTypesComponent name={"Abhijeet"} city={"Mumbai"} />
//             </div>
//         );
//     }
// }

// export default PropTypesRoot;

// ----------------------------------------------------

import React, { Component } from 'react';
import PropTypes from 'prop-types';

// class PropTypesComponent extends Component {
//     render() {
//         return (
//             <div>
//                 <h2 className="text-primary">Hello, {this.props.name.toUpperCase()}</h2>
//                 <h2 className="text-primary">You are from, {this.props.city}</h2>
//             </div>
//         );
//     }

//     // static get propTypes() {
//     //     return {
//     //         name: PropTypes.isRequired,
//     //         city: PropTypes.string.isRequired
//     //     };

//     // }
// }

// PropTypesComponent.propTypes = {
//     name: PropTypes.string.isRequired,
//     city: PropTypes.string.isRequired
// };

function PropTypesComponent({ name = "na", city = "" }) {
    return (
        <div>
            <h2 className="text-primary">Hello, {name.toUpperCase()}</h2>
            <h2 className="text-primary">You are from, {city}</h2>
        </div>
    );
}

PropTypesComponent.propTypes = {
    name: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    products: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })).isRequired
};

class PropTypesRoot extends Component {
    render() {
        return (
            <div>
                <PropTypesComponent name={10} />
            </div>
        );
    }
}

export default PropTypesRoot;